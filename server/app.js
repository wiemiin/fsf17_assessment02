"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var Sequelize = require ("sequelize");

var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

const NODE_PORT = process.env.PORT || 4000;

const SQL_USERNAME="root";
const SQL_PASSWORD="fsf123"
var connection = new Sequelize(
    'grocery',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Grocery = require('./models/grocery_list')(connection, Sequelize);

app.use(express.static(__dirname + "/../client")); 
console.log(__dirname + "/../client"); 

const GROCERY = "/api/grocery"; 
const Op = Sequelize.Op;

//SEARCH
app.get(GROCERY, function(req,res){  
    console.log("Search >" + req.query.keyword);
    var keyword = req.query.keyword; 
    var sortby = req.query.sortby;  
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;


    if(sortby === 'null') {
        sortby = "ASC";
    };
    

    var whereClause = {offset: offset,
        limit: itemsPerPage,
        order: [['brand', sortby],['name', sortby]], 
        where: {
        [Op.or]: [
            {brand: {
                [Op.like]: '%' + keyword + '%'}
            },
            {name: {
                [Op.like]: '%' + keyword + '%'}
            },
                ]
            }}; 

            Grocery.findAndCountAll(whereClause).then(function(results){ 
                res.status(200).json(results); 
                console.log(results);
            }).catch(function(error){
                res.status(500).json(error);
            }); 

  
    
});

//UPDATE
app.put(GROCERY, function(req,res){ 
    console.log(">>>" + JSON.stringify(req.body)); 

    console.log("Update grocery... " + req.body); 
    var id = req.body.id; 
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Grocery.findOne(whereClause).then(function(result){
        result.update(req.body);
        res.status(200).json(result); 
    }).catch(function(error){
        res.status(500).json(error);
    });
});

//RETRIEVE
app.get(GROCERY + "/:upc12", function(req,res){ 
    console.log("One grocery item " + req.params.upc12); 
    
    var upc12 = req.params.upc12;
    console.log(upc12);
    var whereClause = {limit: 1, where: {upc12: upc12}};
    Grocery.findOne(whereClause).then(function(result){
        res.status(200).json(result); 
        console.log(result);
    }).catch(function(error){
        res.status(500).json(error);
    });   
});

//CREATE
app.post(GROCERY, function(req,res){ 
    console.log(">>>" + JSON.stringify(req.body)); 
    
    var grocery = req.body;

    Grocery.create(grocery).then(function(result){ 
        console.log(result);
        res.status(200).json(result);
      }).catch(function(error){
          res.status(500).json(error);
      });

});

app.use(function (req, res) { 
    res.send("<h1>Page Not Found</h1>"); 
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app; 