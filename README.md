# SET UP # 

* git init
* git remote add origin https://wiemiin@bitbucket.org/wiemiin/fsf17_assessment02.git
* mkdir server directory > 
    * app.js
    * models > grocery_list > index.js
* mkdir client directory > 
* app > 
    * app.module.js 
    * app.controller.js 
    * app-route-config.js
    * app.service.js
* css > 
    * main.css
* create .gitignore (add node_modules and client/bower_components)
* npm init (create package.json, enter entry point server/app.js)
* npm install express body-parser sequelize - -save
* npm install - - save mysql2
* bower init (create package.json, enter entry point server/app.js)
* create .bowerrc (directory: "client/bower_components")
* bower install bootstrap font-awesome angular angular-ui-router angular-bootstrap - -save
* insert in index.html >
* core
    * jquery/dist/jquery.js
    * angular/angular.js
    * bootstrap/dist/js/bootstrap.min.js
    * angular-ui-router/release/angular-ui-router.min.js
* add on
    * angular-bootstrap/ui-bootstrap.min.js
    * angular-bootstrap/ui-bootstrap-tpls.min.js
style sheet 
        * ui-bootstrap-csp.css
        * bootstrap/dist/css/bootstrap.min.css
        * font-awesome/css/font-awesome.min.css
        * css/main.css
* inject dependency in app-module.js
    * 'ui.bootstrap'
    * 'ui.router'
* insert in index.html 
    * below app.module.js
    * app-route-config.js


# PUSH TO GIT # 
* git add .
* git commit -m "update"
* git push origin master