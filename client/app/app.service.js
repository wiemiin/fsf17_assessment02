(function() {
    angular
    .module("groceryApp")
    .service("groceryAppAPI", [
        '$http', 
        '$state',
        groceryAppAPI
    ]);

    function groceryAppAPI($http, $state) { 
        var self = this;

       
        self.searchGrocery = function(keyword_value, sortby, itemsPerPage, currentPage) {
            return $http.get(`/api/grocery?keyword=${keyword_value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        } 
        
        self.updateGrocery = function(grocery) { 
            console.log(grocery);
            return $http.put("/api/grocery", grocery);
        }

        self.getGrocery = function(id) {
            console.log(id);
            return $http.get("/api/grocery/" + id) 
        }
        
        self.addGrocery = function(grocery) {
            return $http.post("/api/grocery", grocery);
        }
        
        
    }
    
})();